
package Datos;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;


public class Archivos {
    
     public void GuardarAdminVuelos (String lista) {
        try {
            File archivo = new File("AdminVuelos.txt");
            BufferedWriter archi = new BufferedWriter(new FileWriter(archivo, true));
            archi.write(lista + "\r\n");
            archi.close();
        } catch (Exception e) {
            System.out.println("Error al escribir en el archivo" + e);
        }
    }
     
     public void UsuarioRegistrado (String lista) {
        try {
            File archivo = new File("UsuarioRegistrados.txt");
            BufferedWriter archi = new BufferedWriter(new FileWriter(archivo, true));
            archi.write(lista + "\r\n");
            archi.close();
        } catch (Exception e) {
            System.out.println("Error al escribir en el archivo" + e);
        }
    }
    
}
