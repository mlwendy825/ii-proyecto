
package Negocio;

import Objetos.ObAerolinea;
import Objetos.ObAeropuerto;
import Objetos.ObAvion;
import Objetos.ObTribulacion;
import static Presentacion.Principal.ListaAeropuertos;
import java.util.ArrayList;

public class Listas {
    
    public void llenarAeropuertos(ArrayList<ObAeropuerto> ListaAeropuertos){
        
        ObAeropuerto Aeropuerto1 = new ObAeropuerto("0001", "Aeropuerto Internacional de Barcelona","España");
        ObAeropuerto Aeropuerto2 = new ObAeropuerto("0002", "Aeropuerto Internacional de Palo Negro", "Colombia");
        ObAeropuerto Aeropuerto3 = new ObAeropuerto("0003", "Cozumel International Airport", "Mexico");
        ObAeropuerto Aeropuerto4 = new ObAeropuerto("0004", "Detroit Metro Airport", "Estados Unidos");
        ObAeropuerto Aeropuerto5 = new ObAeropuerto("0005", "Aeropuerto Internacional de Estrasburgo", "Francia");
        ObAeropuerto Aeropuerto6 = new ObAeropuerto("0006", "Aeropuerto Internacional José Joaquín de Olmedo", "Ecuador");
        ObAeropuerto Aeropuerto7 = new ObAeropuerto("0007", "La Chinita International Airport", "Venezuela");
        ObAeropuerto Aeropuerto8 = new ObAeropuerto("0008", "Naha Airport", "Japon");
        ObAeropuerto Aeropuerto9 = new ObAeropuerto("0009", "Aeropuerto Internacional Juan Manuel Gálvez", "Horduras");
        ObAeropuerto Aeropuerto10 = new ObAeropuerto("00010", "Aeropuerto Internacional Juan Santamaría", "Costa Rica");
        
        ListaAeropuertos.add(Aeropuerto1);
        ListaAeropuertos.add(Aeropuerto2);
        ListaAeropuertos.add(Aeropuerto3);
        ListaAeropuertos.add(Aeropuerto4);
        ListaAeropuertos.add(Aeropuerto5);
        ListaAeropuertos.add(Aeropuerto6);
        ListaAeropuertos.add(Aeropuerto7);
        ListaAeropuertos.add(Aeropuerto8);
        ListaAeropuertos.add(Aeropuerto9);
        ListaAeropuertos.add(Aeropuerto10);
        
        for(int i = 0; i < ListaAeropuertos.size(); i++) {
            System.out.println(ListaAeropuertos.get(i).getID());
        }
    }
    
    public void llenarAerolineas(ArrayList<ObAerolinea> ListaAerolineas){
        
        ObAerolinea Aerolinea1 = new ObAerolinea("CMP", "Copa Airlines");
        ObAerolinea Aerolinea2 = new ObAerolinea("EVA", "Eva Airways");
        ObAerolinea Aerolinea3 = new ObAerolinea("OVA", "Aeronova");
        ObAerolinea Aerolinea4 = new ObAerolinea("AJX", "Air Japan");
        ObAerolinea Aerolinea5 = new ObAerolinea("ASH", "Mesa Airlines");
        ObAerolinea Aerolinea6 = new ObAerolinea("AAL", "American Airlines");
        ObAerolinea Aerolinea7 = new ObAerolinea("AVA", "Avianca");
        ObAerolinea Aerolinea8 = new ObAerolinea("ROI", "Avoir Arilines");
        ObAerolinea Aerolinea9 = new ObAerolinea("RSC", "Canarias Airlines");
        ObAerolinea Aerolinea10 = new ObAerolinea("ABX", "Airborne Express");
        
        ListaAerolineas.add(Aerolinea1);
        ListaAerolineas.add(Aerolinea2);
        ListaAerolineas.add(Aerolinea3);
        ListaAerolineas.add(Aerolinea4);
        ListaAerolineas.add(Aerolinea5);
        ListaAerolineas.add(Aerolinea6);
        ListaAerolineas.add(Aerolinea7);
        ListaAerolineas.add(Aerolinea8);
        ListaAerolineas.add(Aerolinea9);
        ListaAerolineas.add(Aerolinea10);
        
        for (int i = 0; i <  ListaAerolineas.size(); i++) {
            System.out.println(ListaAerolineas.get(i));
        }
    }
    
    public void llenarAviones(ArrayList<ObAvion> ListaAviones){
        
        ObAvion Avion1 = new ObAvion(01,"CMP", 60, true);
        ObAvion Avion2 = new ObAvion(02,"CMP", 50, true);
        ObAvion Avion3 = new ObAvion(03,"EVA", 75, true);
        ObAvion Avion4 = new ObAvion(04,"EVA", 60, true);
        ObAvion Avion5 = new ObAvion(05,"OVA", 60, true);
        ObAvion Avion6 = new ObAvion(06,"OVA", 45, true);
        ObAvion Avion7 = new ObAvion(07,"AJX", 60, true);
        ObAvion Avion8 = new ObAvion(10,"AJX", 40, true);
        ObAvion Avion9 = new ObAvion(11,"ASH", 60, true);
        ObAvion Avion10 = new ObAvion(12,"ASH", 55, true);
        ObAvion Avion11 = new ObAvion(13,"AAL", 60, true);
        ObAvion Avion12 = new ObAvion(14,"AAL", 75, true);
        ObAvion Avion13 = new ObAvion(15,"AVA", 60, true);
        ObAvion Avion14 = new ObAvion(16,"AVA", 55, true);
        ObAvion Avion15 = new ObAvion(17,"ROI", 46, true);
        ObAvion Avion16 = new ObAvion(18,"ROI", 58, true);
        ObAvion Avion17 = new ObAvion(19,"RSC", 75, true);
        ObAvion Avion18 = new ObAvion(20,"RSC", 60, true);
        ObAvion Avion19 = new ObAvion(21,"ABX", 52, true);
        ObAvion Avion20 = new ObAvion(22,"ABX", 46, true);
        
        ListaAviones.add(Avion1);
        ListaAviones.add(Avion2);
        ListaAviones.add(Avion3);
        ListaAviones.add(Avion4);
        ListaAviones.add(Avion5);
        ListaAviones.add(Avion6);
        ListaAviones.add(Avion7);
        ListaAviones.add(Avion8);
        ListaAviones.add(Avion9);
        ListaAviones.add(Avion10);
        ListaAviones.add(Avion11);
        ListaAviones.add(Avion12);
        ListaAviones.add(Avion13);
        ListaAviones.add(Avion14);
        ListaAviones.add(Avion15);
        ListaAviones.add(Avion16);
        ListaAviones.add(Avion17);
        ListaAviones.add(Avion18);
        ListaAviones.add(Avion19);
        ListaAviones.add(Avion20);
        
        for (int i = 0; i <  ListaAviones.size(); i++) {
            System.out.println(ListaAviones.get(i));
        }
    }
    
    public void llenarTripulaciones(ArrayList<ObTribulacion> ListaTripulaciones){
        
        ObTribulacion Tripulacion1 = new ObTribulacion(001, "2001", "Fiorella Araya Gutierrez", "CMP", true, true);
        ObTribulacion Tripulacion2 = new ObTribulacion(002, "2002", "Monica Rodriguez Araya", "CMP", true, true);
        ObTribulacion Tripulacion3 = new ObTribulacion(003, "2003", "Mateo Alfaro Molina", "CMP", false, true);
        ObTribulacion Tripulacion4 = new ObTribulacion(004, "2004", "Pablo Herrera Gomez", "CMP", false, true);
        ObTribulacion Tripulacion5 = new ObTribulacion(005, "2005", "Nohelia Cerdas Sanchez", "EVA", true, true);
        ObTribulacion Tripulacion6 = new ObTribulacion(006, "2006", "Jessica Barrantes Zambrano", "EVA", true, true);
        ObTribulacion Tripulacion7 = new ObTribulacion(007, "2007", "Diego Mora Guzman", "EVA", false, true);
        ObTribulacion Tripulacion8 = new ObTribulacion(010, "2008", "Alonso Molina Rojas", "EVA", false, true);  
        ObTribulacion Tripulacion9 = new ObTribulacion(011, "2009", "Carolina Delgado Perez", "OVA", true, true);
        ObTribulacion Tripulacion10 = new ObTribulacion(012, "2010", "Ana Torres Ruiz", "OVA", true, true);
        ObTribulacion Tripulacion11 = new ObTribulacion(013, "2011", "Kevin Guzman Ramos", "OVA", false, true);
        ObTribulacion Tripulacion12 = new ObTribulacion(014, "2012", "Juan Serrano Jimenez", "OVA", false, true);
        ObTribulacion Tripulacion13 = new ObTribulacion(015, "2013", "Michelle Muñoz Fernandez", "AJX", true, true);
        ObTribulacion Tripulacion14 = new ObTribulacion(016, "2014", "Susana Alvarez Suarez", "AJX", true, true);
        ObTribulacion Tripulacion15 = new ObTribulacion(017, "2015", "Luis Navarro Sanchez", "AJX", false, true);
        ObTribulacion Tripulacion16 = new ObTribulacion(020, "2016", "Pedro Mendez Sanz", "AJX", false, true);
        ObTribulacion Tripulacion17 = new ObTribulacion(021, "2017", "Michelle Muñoz Fernandez", "ASH", true, true);
        ObTribulacion Tripulacion18 = new ObTribulacion(022, "2018", "Susana Alvarez Suarez", "ASH", true, true);
        ObTribulacion Tripulacion19 = new ObTribulacion(023, "2019", "Luis Navarro Sanchez", "ASH", false, true);
        ObTribulacion Tripulacion20 = new ObTribulacion(024, "2020", "Pedro Mendez Sanz", "ASH", false, true);
        ObTribulacion Tripulacion21 = new ObTribulacion(025, "2021", "Karen Ortiz Perez", "AAL", true, true);
        ObTribulacion Tripulacion22 = new ObTribulacion(026, "2022", "Fernanda Gonzales Castr", "AAL", true, true);
        ObTribulacion Tripulacion23 = new ObTribulacion(027, "2023", "Daniel Vaquez Delgado", "AAL", false, true);
        ObTribulacion Tripulacion24 = new ObTribulacion(030, "2024", "Max Ortiz Blanco", "AAL", false, true);
        
        ObTribulacion Tripulacion25 = new ObTribulacion(031, "2025", "Dariana Molina Vargass", "AVA", true, true);
        ObTribulacion Tripulacion26 = new ObTribulacion(032, "2026", "Karol Gamboa Marin", "AVA", true, true);
        ObTribulacion Tripulacion27 = new ObTribulacion(033, "2027", "Fabian Rojas Vargass", "AVA", false, true);
        ObTribulacion Tripulacion28 = new ObTribulacion(034, "2028", "Orlando Mora Castillos", "AVA", false, true);
        
        ObTribulacion Tripulacion29 = new ObTribulacion(035, "2029", "Paula Miranda Herrera", "ROI", true, true);
        ObTribulacion Tripulacion30 = new ObTribulacion(036, "2030", "Monica Arrieta Mejias", "ROI", true, true);
        ObTribulacion Tripulacion31 = new ObTribulacion(037, "2031", "Alejandro Muñoz Cerdas", "ROI", false, true);
        ObTribulacion Tripulacion32 = new ObTribulacion(040, "2032", "Darwin Segura Chavarria", "ROI", false, true);
        
        ObTribulacion Tripulacion33 = new ObTribulacion(041, "2033", "Patricia Ruiz Soto", "RSC", true, true);
        ObTribulacion Tripulacion34 = new ObTribulacion(042, "2034", "Lucia Brenes Gomez", "RSC", true, true);
        ObTribulacion Tripulacion35 = new ObTribulacion(043, "2035", "Jorge Chavarria Pineda", "RSC", false, true);
        ObTribulacion Tripulacion36 = new ObTribulacion(044, "2036", "Mario Muñoz Castro", "RSC", false, true);
        
        ObTribulacion Tripulacion37 = new ObTribulacion(045, "20037", "Alex Vera Vega", "ABX", true, true);
        ObTribulacion Tripulacion38 = new ObTribulacion(046, "2038", "Juan Cruz Lopez", "ABX", true, true);
        ObTribulacion Tripulacion39 = new ObTribulacion(047, "2039", "Luis Sanchez Mendoza", "ABX", false, true);
        ObTribulacion Tripulacion40 = new ObTribulacion(050, "2040", "Samael Buendia Perez", "ABX", false, true);
        
        ListaTripulaciones.add(Tripulacion1);
        ListaTripulaciones.add(Tripulacion2);  
        ListaTripulaciones.add(Tripulacion3);  
        ListaTripulaciones.add(Tripulacion4);  
        ListaTripulaciones.add(Tripulacion5); 
        ListaTripulaciones.add(Tripulacion6);
        ListaTripulaciones.add(Tripulacion7);  
        ListaTripulaciones.add(Tripulacion8);  
        ListaTripulaciones.add(Tripulacion9);  
        ListaTripulaciones.add(Tripulacion10);  
        ListaTripulaciones.add(Tripulacion11);  
        ListaTripulaciones.add(Tripulacion12);  
        ListaTripulaciones.add(Tripulacion13);  
        ListaTripulaciones.add(Tripulacion14);  
        ListaTripulaciones.add(Tripulacion15);  
        ListaTripulaciones.add(Tripulacion16);  
        ListaTripulaciones.add(Tripulacion17);  
        ListaTripulaciones.add(Tripulacion18);  
        ListaTripulaciones.add(Tripulacion19);  
        ListaTripulaciones.add(Tripulacion20);  
        ListaTripulaciones.add(Tripulacion21);  
        ListaTripulaciones.add(Tripulacion22);  
        ListaTripulaciones.add(Tripulacion23);  
        ListaTripulaciones.add(Tripulacion24);  
        ListaTripulaciones.add(Tripulacion25);  
        ListaTripulaciones.add(Tripulacion26);  
        ListaTripulaciones.add(Tripulacion27);  
        ListaTripulaciones.add(Tripulacion28);  
        ListaTripulaciones.add(Tripulacion29);  
        ListaTripulaciones.add(Tripulacion30);  
        ListaTripulaciones.add(Tripulacion31);
        ListaTripulaciones.add(Tripulacion32);  
        ListaTripulaciones.add(Tripulacion33);  
        ListaTripulaciones.add(Tripulacion34);  
        ListaTripulaciones.add(Tripulacion35);  
        ListaTripulaciones.add(Tripulacion36);  
        ListaTripulaciones.add(Tripulacion37);  
        ListaTripulaciones.add(Tripulacion38);  
        ListaTripulaciones.add(Tripulacion39);  
        ListaTripulaciones.add(Tripulacion40);  
        
        for (int i = 0; i <  ListaTripulaciones.size(); i++) {
            System.out.println(ListaTripulaciones.get(i));
        }
    }
    
    public void Aeropuertos(){
        for (int i = 0; i <  ListaAeropuertos .size(); i++) {
            System.out.println(ListaAeropuertos .get(i).getID());
        }
    }
                    
}
