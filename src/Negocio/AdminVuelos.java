
package Negocio;

import Datos.Archivos;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;

public class AdminVuelos {
    
    Archivos vuelos = new Archivos ();
    
    public void InsertaAdminVuelos(String lista){        
        vuelos.GuardarAdminVuelos(lista);        
    }
    
    
      public ArrayList<String> LeerDesdeArchivo() {
        ArrayList<String> contenido = new ArrayList<>();
        try {
            File archivo = new File("AdminVuelos.txt");
            BufferedReader archi = new BufferedReader(new FileReader(archivo));
            while (archi.ready()) {
                contenido.add(archi.readLine());
            }            
            archi.close();
        } catch (Exception e) {
            System.out.println("Error al leer desde el archivo" + e);
        }
        return contenido;
     }
    
    
}
