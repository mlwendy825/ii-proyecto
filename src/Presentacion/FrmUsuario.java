/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Presentacion;

import Negocio.AdminVuelos;
import Negocio.Listas;
import Objetos.ObAerolinea;
import Objetos.ObAeropuerto;
import Presentacion.FrmAdmin.*;

import static Presentacion.Principal.ListaAerolineas;
import java.util.ArrayList;
import javax.swing.JOptionPane;
import static Presentacion.Principal.ListaAeropuertos;
import static Presentacion.Principal.llamarListas;


/**
 *
 * @author HP
 */
public class FrmUsuario extends javax.swing.JFrame {
    
    
    FrmAdmin Agreg = new FrmAdmin ();
     AdminVuelos Leer = new AdminVuelos ();
    
    
   
    
    /**
     * Creates new form FrmUsuario
     */
    public FrmUsuario() {
        initComponents();
        AeropuertosCombo();
        
    }
    
 public  void AeropuertosCombo(){
        
        ObAeropuerto Aeropuerto1 = new ObAeropuerto("0001", "Aeropuerto Internacional de Barcelona","España");
        ObAeropuerto Aeropuerto2 = new ObAeropuerto("0002", "Aeropuerto Internacional de Palo Negro", "Colombia");
        ObAeropuerto Aeropuerto3 = new ObAeropuerto("0003", "Cozumel International Airport", "Mexico");
        ObAeropuerto Aeropuerto4 = new ObAeropuerto("0004", "Detroit Metro Airport", "Estados Unidos");
        ObAeropuerto Aeropuerto5 = new ObAeropuerto("0005", "Aeropuerto Internacional de Estrasburgo", "Francia");
        ObAeropuerto Aeropuerto6 = new ObAeropuerto("0005", "Aeropuerto Internacional José Joaquín de Olmedo", "Ecuador");
        ObAeropuerto Aeropuerto7 = new ObAeropuerto("0007", "La Chinita International Airport", "Venezuela");
        ObAeropuerto Aeropuerto8 = new ObAeropuerto("0008", "Naha Airport", "Japon");
        ObAeropuerto Aeropuerto9 = new ObAeropuerto("0009", "Aeropuerto Internacional Juan Manuel Gálvez", "Horduras");
        ObAeropuerto Aeropuerto10 = new ObAeropuerto("00010", "Aeropuerto Internacional Juan Santamaría", "Costa Rica");
        
        ListaAeropuertos.add(Aeropuerto1);
        ListaAeropuertos.add(Aeropuerto2);
        ListaAeropuertos.add(Aeropuerto3);
        ListaAeropuertos.add(Aeropuerto4);
        ListaAeropuertos.add(Aeropuerto5);
        ListaAeropuertos.add(Aeropuerto6);
        ListaAeropuertos.add(Aeropuerto7);
        ListaAeropuertos.add(Aeropuerto8);
        ListaAeropuertos.add(Aeropuerto9);
        ListaAeropuertos.add(Aeropuerto10);
        
        for(int i = 0; i < ListaAeropuertos.size(); i++) {
            CmBoxAeSalida.addItem(ListaAeropuertos.get(i).getID());
            CmBoxAeLlegada.addItem(ListaAeropuertos.get(i).getID());
        }
 }
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        JLAeSalida = new javax.swing.JLabel();
        JLAeLlegada = new javax.swing.JLabel();
        CmBoxAeSalida = new javax.swing.JComboBox<>();
        CmBoxAeLlegada = new javax.swing.JComboBox<>();
        JLDateSalida = new javax.swing.JLabel();
        BTNSiguiente = new javax.swing.JButton();
        jDateChooser1 = new com.toedter.calendar.JDateChooser();
        BtnBuscar = new javax.swing.JButton();
        vuelos = new javax.swing.JComboBox<>();
        jLabel1 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        JLAeSalida.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        JLAeSalida.setText("Aeropuerto de salida");

        JLAeLlegada.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        JLAeLlegada.setText("Aeropuerto de llegada");

        CmBoxAeSalida.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                CmBoxAeSalidaActionPerformed(evt);
            }
        });

        CmBoxAeLlegada.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                CmBoxAeLlegadaActionPerformed(evt);
            }
        });

        JLDateSalida.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        JLDateSalida.setText("Fecha y Hora de salida");

        BTNSiguiente.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        BTNSiguiente.setText("Siguiente");
        BTNSiguiente.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BTNSiguienteActionPerformed(evt);
            }
        });

        BtnBuscar.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        BtnBuscar.setText("Buscar");
        BtnBuscar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnBuscarActionPerformed(evt);
            }
        });

        vuelos.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        jLabel1.setFont(new java.awt.Font("Garamond", 1, 24)); // NOI18N
        jLabel1.setText("Vuelos Disponibles:");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(19, 19, 19)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(JLDateSalida, javax.swing.GroupLayout.PREFERRED_SIZE, 173, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jDateChooser1, javax.swing.GroupLayout.PREFERRED_SIZE, 213, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(JLAeSalida, javax.swing.GroupLayout.PREFERRED_SIZE, 173, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(CmBoxAeSalida, javax.swing.GroupLayout.PREFERRED_SIZE, 213, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(JLAeLlegada, javax.swing.GroupLayout.PREFERRED_SIZE, 173, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(CmBoxAeLlegada, javax.swing.GroupLayout.PREFERRED_SIZE, 213, javax.swing.GroupLayout.PREFERRED_SIZE))))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jLabel1)
                        .addGap(86, 86, 86)
                        .addComponent(BtnBuscar))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(BTNSiguiente, javax.swing.GroupLayout.PREFERRED_SIZE, 243, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(vuelos, javax.swing.GroupLayout.PREFERRED_SIZE, 551, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap(414, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(16, 16, 16)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(JLAeSalida, javax.swing.GroupLayout.PREFERRED_SIZE, 19, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(CmBoxAeSalida, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(33, 33, 33)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(JLAeLlegada, javax.swing.GroupLayout.PREFERRED_SIZE, 19, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(CmBoxAeLlegada, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(16, 16, 16)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(JLDateSalida, javax.swing.GroupLayout.PREFERRED_SIZE, 19, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jDateChooser1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 51, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(BtnBuscar))
                .addGap(18, 18, 18)
                .addComponent(vuelos, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(40, 40, 40)
                .addComponent(BTNSiguiente))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(22, 22, 22)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(25, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void BTNSiguienteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BTNSiguienteActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_BTNSiguienteActionPerformed

    private void CmBoxAeSalidaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_CmBoxAeSalidaActionPerformed
      
       
    }//GEN-LAST:event_CmBoxAeSalidaActionPerformed

    private void CmBoxAeLlegadaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_CmBoxAeLlegadaActionPerformed
       
    }//GEN-LAST:event_CmBoxAeLlegadaActionPerformed

    private void BtnBuscarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnBuscarActionPerformed

         String valorSalida =  CmBoxAeSalida.getSelectedItem().toString();
        String valorLlegada =  CmBoxAeLlegada.getSelectedItem().toString(); 
        
        if (valorSalida == valorLlegada){
            JOptionPane.showMessageDialog(null, "Los Aeropuertos selecionados deben ser diferentes");
            CmBoxAeSalida.setSelectedIndex(-1);
            CmBoxAeSalida.setSelectedIndex(-1);
        }
        else {
            if (CmBoxAeSalida.getItemCount() == 2)
                    {
                         JOptionPane.showMessageDialog(null, "Vuelo sin escala");
                    }
        ArrayList<String> impresion = Leer.LeerDesdeArchivo();
        
        for (String datos : impresion)
        {
            vuelos.addItem(String.valueOf(datos + "\r\n"));
        }
        }

    }//GEN-LAST:event_BtnBuscarActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton BTNSiguiente;
    private javax.swing.JButton BtnBuscar;
    private javax.swing.JComboBox<String> CmBoxAeLlegada;
    private javax.swing.JComboBox<String> CmBoxAeSalida;
    private javax.swing.JLabel JLAeLlegada;
    private javax.swing.JLabel JLAeSalida;
    private javax.swing.JLabel JLDateSalida;
    private com.toedter.calendar.JDateChooser jDateChooser1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JComboBox<String> vuelos;
    // End of variables declaration//GEN-END:variables
}
