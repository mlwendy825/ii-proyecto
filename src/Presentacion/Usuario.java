/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Presentacion;

import java.util.Vector;

/**
 *
 * @author W.Moreno
 */
public class Usuario {
    
    String Nombre;
    String Ced;
    String Contra;
    String Tipo;
    int edad;

    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String Nombre) {
        this.Nombre = Nombre;
    }

    public String getCed() {
        return Ced;
    }

    public void setCed(String Ced) {
        this.Ced = Ced;
    }

    public String getContra() {
        return Contra;
    }

    public void setContra(String Contra) {
        this.Contra = Contra;
    }

    public String getTipo() {
        return Tipo;
    }

    public void setTipo(String Tipo) {
        this.Tipo = Tipo;
    }

    public int getEdad() {
        return edad;
    }
    
    public static int verificar (String ced)
    {
        Vector lista = mostrar ();
        Usuario obj;
        for (int i = 0; i < lista.size(); i++)
        {
            obj = (Usuario) lista.elementAt(i);
            if (obj.getCed().equalsIgnoreCase(ced))
            {
                return i;
            }
        }
        return -1;
    }
    public static int verificarinicio (String Ced, String Contra)
    {
         Vector lista = mostrar ();
        Usuario obj;
        for (int i = 0; i < lista.size(); i++)
        {
            obj = (Usuario) lista.elementAt(i);
            if (obj.getCed().equalsIgnoreCase(Ced) && obj.getContra().equalsIgnoreCase(Contra))
            {
                return i;
            }
        }
        return -1;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }
    public static Vector mostrar ()
    {
        return ListaUsuario.mostrar();
    }
}
