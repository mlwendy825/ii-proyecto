
package Presentacion;

import Negocio.Listas;
import Objetos.*;
import java.util.ArrayList;

public class Principal {
    
    public static Listas llamarListas = new Listas();
    public static ArrayList<ObAeropuerto> ListaAeropuertos = new ArrayList<ObAeropuerto>();
    public static ArrayList<ObAerolinea> ListaAerolineas = new ArrayList<ObAerolinea>();
    public static ArrayList<ObAvion> ListaAviones = new ArrayList<ObAvion>();
    public static ArrayList<ObTribulacion> ListaTripulaciones = new ArrayList<ObTribulacion>();
    
     public static void main(String[] args) {
         
        llamarListas.llenarAeropuertos(ListaAeropuertos);
        llamarListas.llenarAerolineas(ListaAerolineas);
        llamarListas.llenarAviones(ListaAviones);
        llamarListas.llenarTripulaciones(ListaTripulaciones);
         
        FrmLoginRegistro LoginRegistro = new FrmLoginRegistro();
        LoginRegistro.setVisible(true);

    }
    
}
