
package Objetos;

public class ObAeropuerto {
    
    private String ID;
    private String Nombre;
    private String Pais;

    public ObAeropuerto() {
    }

    public ObAeropuerto(String ID, String Nombre, String Pais) {
        this.ID = ID;
        this.Nombre = Nombre;
        this.Pais = Pais;
    }

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String Nombre) {
        this.Nombre = Nombre;
    }

    public String getPais() {
        return Pais;
    }

    public void setPais(String Pais) {
        this.Pais = Pais;
    }

    @Override
    public String toString() {
        return this.ID+", "+this.Nombre+", "+this.Pais;
    }
    
}
