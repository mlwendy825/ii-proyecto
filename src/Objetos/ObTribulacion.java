
package Objetos;

public class ObTribulacion {
    
    private int ID;
    private String Cedula;
    private String Nombre;
    private String IDAerolinea;
    private boolean Rol;
    private boolean Estado;

    public ObTribulacion() {
    }

    public ObTribulacion(int ID, String Cedula, String Nombre, String IDAerolinea, boolean Rol, boolean Estado) {
        this.ID = ID;
        this.Cedula = Cedula;
        this.Nombre = Nombre;
        this.IDAerolinea = IDAerolinea;
        this.Rol = Rol;
        this.Estado = Estado;
    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public String getCedula() {
        return Cedula;
    }

    public void setCedula(String Cedula) {
        this.Cedula = Cedula;
    }

    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String Nombre) {
        this.Nombre = Nombre;
    }

    public String getIDAerolinea() {
        return IDAerolinea;
    }

    public void setIDAerolinea(String IDAerolinea) {
        this.IDAerolinea = IDAerolinea;
    }

    public boolean isRol() {
        return Rol;
    }

    public void setRol(boolean Rol) {
        this.Rol = Rol;
    }

    public boolean isEstado() {
        return Estado;
    }

    public void setEstado(boolean Estado) {
        this.Estado = Estado;
    }
    
    @Override
    public String toString() {
        return this.ID+", "+this.Cedula+","+this.Nombre+","+this.IDAerolinea+", "+this.Rol+", "+this.Estado;
    }
    
}
