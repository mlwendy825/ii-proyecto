
package Objetos;

public class ObAerolinea {
    
    String ID;
    String Nombre;

    public ObAerolinea() {
    }

    public ObAerolinea(String ID, String Nombre) {
        this.ID = ID;
        this.Nombre = Nombre;
    }

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String Nombre) {
        this.Nombre = Nombre;
    }
    
    @Override
    public String toString() {
        return this.ID+", "+this.Nombre;
    }
    
}
