/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Datos;

import java.util.Date;

/**
 *
 * @author W.Moreno
 */
public class ObVuelos {
    
private String Aerolínea;
private String Precio;
private Date FHsalida; 
private String Aerosalida;
private Date FHorallegada; 
private String Aerollegada;

  public ObVuelos() {
    }
  public ObVuelos (String Aerolínea, String Precio, Date FHsalida, String Aerosalida, Date FHorallegada, String Aerollegada) {
        this.Aerolínea = Aerolínea;
        this.Precio = Precio;
        this.FHsalida = FHsalida;
        this.Aerosalida = Aerosalida;
        this.FHorallegada = FHorallegada;
        this.Aerollegada = Aerollegada;}

    
    public String getAerolínea() {
        return Aerolínea;
    }

    public void setAerolínea(String Aerolínea) {
        this.Aerolínea = Aerolínea;
    }

    public String getPrecio() {
        return Precio;
    }

    public void setPrecio(String Precio) {
        this.Precio = Precio;
    }

    public Date getFHsalida() {
        return FHsalida;
    }

    public void setFHsalida(Date FHsalida) {
        this.FHsalida = FHsalida;
    }

    public String getAerosalida() {
        return Aerosalida;
    }

    public void setAerosalida(String Aerosalida) {
        this.Aerosalida = Aerosalida;
    }

    public Date getFHorallegada() {
        return FHorallegada;
    }

    public void setFHorallegada(Date FHorallegada) {
        this.FHorallegada = FHorallegada;
    }

    public String getAerollegada() {
        return Aerollegada;
    }

    public void setAerollegada(String Aerollegada) {
        this.Aerollegada = Aerollegada;
    }
    
     @Override
    public String toString() {
        return this.Aerollegada+", "+this.Aerolínea+", "+this.Aerosalida
        
        +", "+this.FHorallegada+", "+this.FHsalida+", "+this.Precio;
    }
    
}
