
package Objetos;

public class ObAvion {
    
    private int ID;
    private String IDAerolinea;
    private int Capacidad;
    private boolean Estado;

    public ObAvion() {
    }

    public ObAvion(int ID, String IDAerolinea, int Capacidad, boolean Estado) {
        this.ID = ID;
        this.IDAerolinea = IDAerolinea;
        this.Capacidad = Capacidad;
        this.Estado = Estado;
    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public String getIDAerolinea() {
        return IDAerolinea;
    }

    public void setIDAerolinea(String IDAerolinea) {
        this.IDAerolinea = IDAerolinea;
    }

    public int getCapacidad() {
        return Capacidad;
    }

    public void setCapacidad(int Capacidad) {
        this.Capacidad = Capacidad;
    }

    public boolean isEstado() {
        return Estado;
    }

    public void setEstado(boolean Estado) {
        this.Estado = Estado;
    }
    
    @Override
    public String toString() {
        return this.ID+", "+this.IDAerolinea+", "+this.Capacidad+", "+this.Estado;
    }
    
}
